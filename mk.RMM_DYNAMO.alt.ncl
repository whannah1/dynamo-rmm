load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_code.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/gsn_csm.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/contributed.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/csm/shea_util.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/time_axis_labels.ncl"
load "$NCARG_ROOT/lib/ncarg/nclscripts/contrib/cd_string.ncl"
load "$NCARG_ROOT/custom_functions_hindcast.ncl"
load "$NCARG_ROOT/custom_functions_obs.ncl"
begin
	
	top_dir = "/Users/whannah/"
	
	fac   = 1		; Samples per day
	num_h = 3		; number of harmonics for seasonal cycle
	
	mkplot = True
	
	sigma1 = sqrt(55.43719)
	sigma2 = sqrt(52.64146)
	
	wind_only = False 
	
	yr = 31	; 2011
	
	wind_only = False
	olr_only  = False
	
	odir  = "/Users/whannah/DYNAMO/"
	;ofile = odir+"RMM_DYNAMO.alt.txt"
	if (.not.wind_only).and.(.not.olr_only) then	ofile = odir+"RMM_DYNAMO.w-inter-ann.alt.txt"end if
	if wind_only then 							ofile = odir+"RMM_DYNAMO.w-inter-ann.wind_only.txt" end if
	if olr_only  then 							ofile = odir+"RMM_DYNAMO.w-inter-ann.olr_only.txt"  end if
;===================================================================================
; Coordinates
;===================================================================================
  if wind_only.and.olr_only then
    print("ERROR: you can specify either wind_only or olr_only, but not both!")
    exit
  end if
  lat1 = -15.
  lat2 =  15.
  lon1 =   0.
  lon2 = 360.
  ifile  = top_dir+"/DYNAMO/NCEP/NCEP.U850.1980-2011.nc"
  infile = addfile(ifile,"r")
  lat = infile->lat({lat1:lat2})
  lon = infile->lon
  num_lat = dimsizes(lat)
  num_lon = dimsizes(lon)
;===================================================================================
; Load seasonal cycle data
;===================================================================================
      ifile = top_dir+"/DYNAMO/NCEP/NCEP.RMM.seasonal_cycle.1980-2010.nc"
      infile = addfile(ifile,"r")
      HU850 = dim_sum_n_Wrap(infile->HU850(:num_h-1,0:,{lat1:lat2},{lon1:lon2}),0)
      HU200 = dim_sum_n_Wrap(infile->HU200(:num_h-1,0:,{lat1:lat2},{lon1:lon2}),0)
      HOLR  = dim_sum_n_Wrap(infile->HOLR (:num_h-1,0:,{lat1:lat2},{lon1:lon2}),0)
;===================================================================================
; Load historical RMM data from Matt Wheeler's website
;===================================================================================  
    data = asciiread(top_dir+"/DYNAMO/RMM.no_leap.txt",(/365*(yr+1),7/),"float")    
    oRMM1 = data(365*yr:365*(yr+1)-1,3)
	oRMM2 = data(365*yr:365*(yr+1)-1,4)
  	delete([/data/])
    data = asciiread(top_dir+"/DYNAMO/RMM.no_leap.w-inter-ann.txt",(/365*(yr+1),7/),"float")    
	roRMM1 = data(365*yr:365*(yr+1)-1,3)
	roRMM2 = data(365*yr:365*(yr+1)-1,4)
  	delete([/data/])
;===================================================================================
; Caclulate RMM
;===================================================================================
	if .not.olr_only then
      ;----------------------------------------
      print("  loading U850")
      infile = addfile(top_dir+"/DYNAMO/NCEP/NCEP.U850.1980-2011.nc","r")
      itmp = infile->U(:,{lat1:lat2},{lon1:lon2})
      iUlo = ( itmp(0::4,:,:) +itmp(1::4,:,:)+itmp(2::4,:,:)+itmp(3::4,:,:) )/4.
        delete([/itmp/])
      ;----------------------------------------
      print("  loading U200")
      infile = addfile(top_dir+"/DYNAMO/NCEP/NCEP.U200.1980-2011.nc","r")
      itmp = infile->U(:,{lat1:lat2},{lon1:lon2})
      iUhi = ( itmp(0::4,:,:) +itmp(1::4,:,:)+itmp(2::4,:,:)+itmp(3::4,:,:) )/4.
        delete([/itmp/])
      ;----------------------------------------
    end if
      ;----------------------------------------
      print("  loading OLR")
      infile = addfile(top_dir+"/DYNAMO/OLR/olr.daily.1980-2011.nc","r")
      itmp = infile->OLR(:,{lat1:lat2},{lon1:lon2})
      iOLR = itmp(:,{lat1:lat2},{lon1:lon2})
        delete([/itmp/])
      ;----------------------------------------
      if olr_only then
        iUlo = new(dimsizes(iOLR),float)
        iUhi = new(dimsizes(iOLR),float)
      end if
      print("  done.")
    ;================================================================
    ; Remove mean
    ;================================================================
      myr = 21
      tempvar = new((/365,num_lat,num_lon/),float)
      Ulo = iUlo(yr*365:(yr+1)*365-1,:,:) - conform(tempvar,dim_avg_n(iUlo(:myr*365-1,:,:),0),(/1,2/))
      Uhi = iUhi(yr*365:(yr+1)*365-1,:,:) - conform(tempvar,dim_avg_n(iUhi(:myr*365-1,:,:),0),(/1,2/))
      OLR = iOLR(yr*365:(yr+1)*365-1,:,:) - conform(tempvar,dim_avg_n(iOLR(:myr*365-1,:,:),0),(/1,2/))
      
      iUlo = iUlo - conform(iUlo,dim_avg_n(iUlo(:myr*365-1,:,:),0),(/1,2/))
      iUhi = iUhi - conform(iUhi,dim_avg_n(iUhi(:myr*365-1,:,:),0),(/1,2/))
      iOLR = iOLR - conform(iOLR,dim_avg_n(iOLR(:myr*365-1,:,:),0),(/1,2/))
    ;================================================================
    ; Remove observed climatological seaonal cycle
    ;================================================================
      print("  Removing seasonal cycle...")
      ;----------------------------------------
      ; Load NCEP seasonal cycle climatology
      ;----------------------------------------
      ifile = top_dir+"/DYNAMO/NCEP/NCEP.RMM.seasonal_cycle.1980-2010.nc"
      infile = addfile(ifile,"r")
      HUlo = dim_sum_n_Wrap(infile->HU850(:num_h-1,:,{lat1:lat2},{lon1:lon2}),0) 
      HUhi = dim_sum_n_Wrap(infile->HU200(:num_h-1,:,{lat1:lat2},{lon1:lon2}),0) 
      HOLR = dim_sum_n_Wrap(infile->HOLR (:num_h-1,:,{lat1:lat2},{lon1:lon2}),0) 
      ;----------------------------------------
      ; Subtract seasonal cycle from hindcast
      ;----------------------------------------
      Ulo = (/ Ulo - HUlo /)
      Uhi = (/ Uhi - HUhi /)
      ;OLR = (/ OLR - HOLR /)
      
      do y = 0,1
       iUlo((yr-y)*365:(yr+1-y)*365-1,:,:) = (/ iUlo((yr-y)*365:(yr+1-y)*365-1,:,:) - HUlo /)
       iUhi((yr-y)*365:(yr+1-y)*365-1,:,:) = (/ iUhi((yr-y)*365:(yr+1-y)*365-1,:,:) - HUhi /)
       iOLR((yr-y)*365:(yr+1-y)*365-1,:,:) = (/ iOLR((yr-y)*365:(yr+1-y)*365-1,:,:) - HOLR /)
      end do
      print("  done.")
    ;================================================================
    ; Remove 120-mean (interannual
    ;================================================================
      rUlo = Ulo
      rUhi = Uhi
      rOLR = OLR
      mlen  = 119
      d1 = yr*365
      do t = 0,364
        Ulo(t,:,:) = (/ Ulo(t,:,:) - dim_avg_n(iUlo(d1+t-mlen:d1+t,:,:),0) /)
        Uhi(t,:,:) = (/ Uhi(t,:,:) - dim_avg_n(iUhi(d1+t-mlen:d1+t,:,:),0) /)
        OLR(t,:,:) = (/ OLR(t,:,:) - dim_avg_n(iOLR(d1+t-mlen:d1+t,:,:),0) /)
      end do
    ;================================================================
    ; Average Across the Equator
    ;================================================================
      meUlo = dim_avg_n(Ulo,1) /  1.81		; interannual removed
      meUhi = dim_avg_n(Uhi,1) /  4.81
      meOLR = dim_avg_n(OLR,1) / 15.12
      
      reUlo = dim_avg_n(rUlo,1) /  1.81		; interannual retained
      reUhi = dim_avg_n(rUhi,1) /  4.81
      reOLR = dim_avg_n(rOLR,1) / 15.12
  ;================================================================
  ; Create Vector for projection
  ;================================================================
    mV = new((/365,3*num_lon/),float)
    rV = mV
    
    if (.not.wind_only).and.(.not.olr_only) then
      i = 0
      mV(:,i:i+num_lon-1) = meOLR
      rV(:,i:i+num_lon-1) = reOLR
      i = num_lon
      mV(:,i:i+num_lon-1) = meUlo
      rV(:,i:i+num_lon-1) = reUlo
      i = num_lon*2
      mV(:,i:i+num_lon-1) = meUhi
      rV(:,i:i+num_lon-1) = reUhi
    end if
    if wind_only then
      i = num_lon
      mV(:,i:i+num_lon-1) = meUlo
      rV(:,i:i+num_lon-1) = reUlo
      i = num_lon*2
      mV(:,i:i+num_lon-1) = meUhi
      rV(:,i:i+num_lon-1) = reUhi
    end if
    if olr_only then
      i = 0
      mV(:,i:i+num_lon-1) = meOLR
      rV(:,i:i+num_lon-1) = reOLR
    end if
  ;================================================================
  ; Project onto EOFs
  ;================================================================
    ifile = top_dir+"/DYNAMO/Hindcast/RMM_EOF_Structures.txt"
    data = asciiread(ifile,23+144*2*3,"float")
    EOF1 = data(23::2) 
    EOF2 = data(24::2) 
    
    ; interannual removed
    mRMM1 = dim_sum_n(mV*conform(mV,EOF1,1),1) / sigma1	
    mRMM2 = dim_sum_n(mV*conform(mV,EOF2,1),1) / sigma2
    mAMP = sqrt(mRMM1^2.  +mRMM2^2.)
	mPHS  = atan2(mRMM2  ,mRMM1)
    
    ; interannual retained
    rRMM1 = dim_sum_n(rV*conform(rV,EOF1,1),1) / sigma1	
    rRMM2 = dim_sum_n(rV*conform(rV,EOF2,1),1) / sigma2
    rAMP = sqrt(rRMM1^2.  +rRMM2^2.)
    rPHS  = atan2(rRMM2  ,rRMM1)
    
    print(rRMM1+"	"+rRMM2+"	"+rAMP)
  ;================================================================
  ; Write to file
  ;================================================================
    num_t = 30+31+30+31
    mn = new(num_t,string)
    dy = new(num_t,string)
    		t1 = 0
    		t2 = 30-1
    		mn(t1:t2) 	= "09"
    		dy(t1:t2)	= sprinti("%0.2i",ispan(1,30,1))
    		t1 = 30
    		t2 = 30+31-1
    		mn(t1:t2) 	= "10"
    		dy(t1:t2)	= sprinti("%0.2i",ispan(1,31,1))
    		t1 = 30+31
    		t2 = 30+31+30-1
    		mn(t1:t2) 	= "11"
    		dy(t1:t2)	= sprinti("%0.2i",ispan(1,30,1))
    		t1 = 30+31+30
    		t2 = 30+31+30+31-1
    		mn(t1:t2) 	= "12"
    		dy(t1:t2)	= sprinti("%0.2i",ispan(1,31,1))
    rt1 = 31+28+31+30+31+30+31+31
    lines = new(num_t,string)
    do t = 0,num_t-1
      lines(t) = "     2011     "+mn(t)+"     "+dy(t)+"     "+  \
                 rRMM1(rt1+t)+"     "+rRMM2(rt1+t)+"          "+		\
                 rPHS (rt1+t)+"     "+rAMP (rt1+t)
    end do
    ;print(lines)
    ;asciiwrite(ofile, lines)
    asciiwrite(ofile, lines(30:))
    
    print("")
    print("	"+ofile)
    print("")
    exit;
  ;================================================================
  ; plotting code for sanity check
  ;================================================================
	if mkplot then
	  d1 = 0;31+28+31+30+31+30+31+30+30
	  ot1 = 31+28+31+30+31+30+31+30
	  ot2 = 364
	  mst = fac
	  wks = gsn_open_wks("png","test.RMM_DYNAMO.alt")
	  plot = new(3,graphic)
		res = True
		res@gsnDraw = False
		res@gsnFrame = False
		res@vpHeightF = 0.3
		res@xyDashPatterns    = (/0,1,0,1/)
		res@xyLineColors   	  = (/"black","blue","red","orange"/)
		res@xyLineThicknesses = (/4.,1.,1.,1./)
	  if isvar("mAMP") then
	    delete(mAMP)
	  end if
	  if isvar("rAMP") then
	    delete(rAMP)
	  end if
	  oAMP  = sqrt(oRMM1 (ot1:)^2.+oRMM2 (ot1:)^2.)
	  roAMP = sqrt(roRMM1(ot1:)^2.+roRMM2(ot1:)^2.)
	  mAMP  = sqrt(mRMM1 (ot1:)^2.+mRMM2 (ot1:)^2.)
	  rAMP  = sqrt(rRMM1 (ot1:)^2.+rRMM2 (ot1:)^2.)
	  
	  if isvar("mPHS") then
	    delete(mPHS)
	  end if
	  if isvar("rPHS") then
	    delete(rPHS)
	  end if
	  oPHS  = atan2(oRMM2 (ot1:) ,oRMM1 (ot1:))
	  roPHS = atan2(roRMM2(ot1:) ,roRMM1(ot1:))
	  mPHS  = atan2(mRMM2 (ot1:) ,mRMM1 (ot1:))
	  rPHS  = atan2(rRMM2 (ot1:) ,rRMM1 (ot1:))
print(oAMP+"		"+oRMM1(ot1:)+"		"+oRMM2(ot1:))
	  res@gsnRightString = "AMP"
	  plot(0) = gsn_csm_y(wks,(/oAMP,roAMP,mAMP,rAMP/),res)
	  res@gsnRightString = "PHS"
	  plot(1) = gsn_csm_y(wks,(/oPHS,roPHS,mPHS,rPHS/),res)
	  ;res@gsnRightString = "RMM1"
	  ;plot(1) = gsn_csm_y(wks,(/oRMM1,roRMM1,mRMM1,rRMM1/),res)
	  ;res@gsnRightString = "RMM2"
	  ;plot(2) = gsn_csm_y(wks,(/oRMM2,roRMM2,mRMM2,rRMM2/),res)
	  gsn_panel(wks,plot(0:1),(/2,1/),False)
	end if
	
	;print("test.RMM_DYNAMO.alt")
	
;===================================================================================
;===================================================================================
end
